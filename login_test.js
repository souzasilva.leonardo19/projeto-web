Feature('login');

Scenario('Login com sucesso',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#user','teste@gmail.com')
    I.fillField('#password','12345678')
    I.click('#btnLogin')
    I.waitForText('Login realizado',5)

}).tag('@sucesso')

Scenario('Tentando Logar digitando apenas o e-mail',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#user','teste@gmail.com')
    I.fillField('#password','')
    I.click('#btnLogin')
    I.waitForText('Senha inválida.',3)

}).tag('@CampoEmailVazio')

Scenario('Tentando logar sem digitar e-mail e senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#user','')
    I.fillField('#password','')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.',3)

}).tag('@CamposVazios')

Scenario('Tentando Logar digitando apenas a senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#user','')
    I.fillField('#password','12345678')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.',3)

}).tag('@CampoSenhaVazio')
